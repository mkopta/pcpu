package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"strconv"
	"strings"
	"time"
)

func is_running(pid int) bool {
	stat, err := ioutil.ReadFile(fmt.Sprintf("/proc/%d/stat", pid))
	if err != nil {
		log.Fatal(err)
	}
	state := strings.Fields(fmt.Sprintf("%s", stat))[2]
	return state == "R"
}

func measure_cpu_usage(pid int, c chan []int) {
	cpu_usage := 0
	for i := 0; i < 100; i++ {
		if is_running(pid) {
			cpu_usage++
		}
		time.Sleep(10 * time.Millisecond)
	}
	measurement := make([]int, 2)
	measurement[0] = pid
	measurement[1] = cpu_usage
	c <- measurement
}

func main() {
	if len(os.Args) == 1 {
		fmt.Printf("Usage: %s <pid> [<pid> ..]\n", os.Args[0])
		return
	}
	c := make(chan []int)
	pcpu := make(map[int]int)
	pids := make([]int, len(os.Args) - 1)
	for i := 1; i < len(os.Args); i++ {
		pid, err := strconv.Atoi(os.Args[i])
		if err != nil {
			log.Fatal(err)
		}
		pids[i - 1] = pid
		go measure_cpu_usage(pid, c)
	}
	for i := 1; i < len(os.Args); i++ {
		measurement := <-c
		pcpu[measurement[0]] = measurement[1]
	}
	for _, pid := range pids {
		fmt.Printf("%d:%d\n", pid, pcpu[pid])
	}
}
